package ru.t1.chernysheva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.service.IAuthService;
import ru.t1.chernysheva.tm.api.service.IUserService;
import ru.t1.chernysheva.tm.command.AbstractCommand;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.exception.entity.UserNotFoundException;
import ru.t1.chernysheva.tm.model.User;

import java.util.List;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @NotNull
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    protected void renderUsers(@Nullable final List<User> users) {
        users.forEach(user -> showUser(user));
    }

    protected void showUser(@Nullable final User user) throws RuntimeException {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LOCKED: " + user.getLocked());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
