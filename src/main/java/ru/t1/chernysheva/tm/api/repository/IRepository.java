package ru.t1.chernysheva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    interface IRepositoryOptional<M extends AbstractModel> {

        Optional<M> findOneById(String id);

        Optional<M> findOneByIndex(Integer index);

    }

    default IRepositoryOptional<M> optional() {
        return new IRepositoryOptional<M>() {
            @Override
            public Optional<M> findOneById(final String id) {
                return Optional.ofNullable(IRepository.this.findOneById(id));
            }
            @Override
            public Optional<M> findOneByIndex(final Integer index) {
                return Optional.ofNullable(IRepository.this.findOneByIndex(index));
            }
        };
    }

    void clear();

    @Nullable
    M add(M model);

    @Nullable
    List<M> findAll();

    @Nullable
    List<M> findAll(@Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    boolean existsById(@NotNull String id);

    int getSize();

    @Nullable
    M remove(M model);

    @Nullable
    M removeById(@Nullable String id);

    @Nullable
    M removeByIndex(@NotNull Integer index);

    void removeAll(Collection<M> collection);

}
