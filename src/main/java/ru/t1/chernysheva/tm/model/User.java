package ru.t1.chernysheva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractModel {

    @NonNull
    private String login;

    @NonNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NonNull
    private Role role = Role.USUAL;

    @NonNull
    private Boolean locked = false;

}
